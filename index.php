<!doctype html>
<!--[if IE 7]>    <html class="ie7" > <![endif]-->
<!--[if IE 8]>    <html class="ie8" > <![endif]-->
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 9]>    <html class="ie10" > <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!--> <html lang="en-US"> <!--<![endif]-->
		<head>
				<!-- META TAGS -->
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width" />
				
				<!-- Title -->
				<title>China-Travel</title>
				
				<!-- Style Sheet-->
                <link rel="stylesheet" href="css/bootstrap.css">
				<link rel="stylesheet" href="css/style.css">
				<link rel="stylesheet" href="css/responsive.css">
                <link rel="stylesheet" href="css/flexslider.css">
				
               <!-- favicon -->
				<link rel="shortcut icon" href="images/favicon.png">
				
				<!--[if lt IE 9]>
						<script src="js/html5shiv.js"></script>
						<link rel="stylesheet" href="css/ie.css">
				<![endif]-->
		</head>
		<body>				
				<!-- HEADER TOP -->
                <div id="top"></div>
				<div class="header-one">
					<div class="headertop-wrapper">
						<div class="container">
					    		
					            <div class="row">
					            	<div class="span5 clearfix">
					                	<div class="lang pull-right">
					                    	<span>Select a language:<a href="#">English</a></span>
					                    </div>
					                </div>
					                <div class="span4 srch">
                                        <form>
                                            <input type="submit" value="">
                                            <input type="text" name="search" placeholder="Search">
                                        </form>
					                </div>
					                <div class="span3">
					                	<div class="social-nav">
					                    	<a href="#" class="facebook "></a>
					                    	<a href="#" class="twitter" ></a>
					                    	<a href="#" class="google"></a>
					                    	<a href="#" class="rss"></a>
					                    </div>
                                        <a href="#" class="sign-in" id="login-link">Sign in</a>
					                </div>
					            </div>
					    </div>
					</div>
				</div>
				<!-- HEADER IOP -->

				<!-- HEADER -->
                <div class="header-wrapper">
                	<div class="container">
                    	<div class="row">
                        
                        	<!-- Logo -->
                            <div class="span4">
                            	<div class="logo">
                                	<a href="index.php"><img src="images/temp.png" alt="Logo">China-Railway</a>
                                </div>
                            </div>
                        	<!-- Logo -->
                            
                            <!--top Menu -->
                            <div class="span8">
                            	<div class="top-menu">
                                	<ul>
                                		<li><a href="#">About US</a></li>
                                		<li><a href="#">News</a></li>
                                		<li><a href="#">Serbice</a></li>
                                		<li><a href="#">Recruiment</a></li>
                                		<li><a href="#">Media</a></li>
                                		<li><a href="#" class="last">Support</a></li>
                                	</ul>
                                </div>
                            </div>
                            <!--top Menu -->
                        
                        </div>
                    </div>
                </div>
                <!-- HEADER -->
                <div class="copyrights">Collect from <a href="http://www.cssmoban.com/" >免费模板</a></div>

                <!-- Main Navigation -->
                <div class="nav-wrapper">
                	<div class="container">
                    	<div class="row">
                        	<div class="span12">
                            
                            	<nav>
                                    <ul>
                                        <li><a href="index.php">Home </a>
                                        	
                                        </li>
                                        <li><a href="trainlist.php">train list</a>
                                       	  
                                        </li>
                                     
                                        
                                      
                                      
                                      
                                      <li><a href="login.php">Manage</a></li>
                                        <li><a href="payment.php">Booking</a>
                                       	  
                                        </li>
                                        <li  class="last"><a href="#111">Contact</a></li>
                                    </ul>
                                </nav>

                                <div class="responsive_nav">
                                    <ul>
                                        <li class="open">
                                            <a href="#">HOME</a>
                                            <ul>
                                                <li><a href="#">Home </a></li>
                                                <li><a href="#">Hotels</a></li>
                                                <li><a href="#">Holidays</a></li>
                                                <li><a href="#">Flights</a> </li>
                                                <li><a href="#">Camera</a></li>
                                                <li><a href="#">Notebook </a></li>
                                                <li><a href="#">Tablet </a> </li>
                                                <li><a href="#">Television </a> </li>
                                                <li><a href="#">Smart Phone </a> </li>
                                                <li><a href="#">Projection </a> </li>
                                                <li><a href="#">Cars</a></li>
                                                <li><a href="#">Vacations</a></li>
                                                <li><a href="#">Guide Book</a></li>
                                                <li><a href="#">Hot Deal</a></li>
                                                <li><a href="#">Cruise</a></li>
                                                <li class="last"><a href="#">Promotion</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Main Navigation -->

                <!-- Slider -->
				<div class="flexslider">
                	<ul class="slides">
                        <li>
                          <img src="Images/train1.jpg" alt="Slider Image">
                          <div class="detail-one">
                           
                          
                          </div>
                        </li>
                        <li>
                          <img src="Images/train2.jpg" alt="Slider Image">
                          <div class="detail-one">
                           
                          </div>
                        </li>
                    </ul>
                    
                    
                    <!-- Reservation box -->
                    <div id="accordion">

                      <h3> <span class="#">Train</span> <a href="#"></a> </h3>
                      <div class="detail">
                         <form action="#" method="post">
                            <div class="trip">
                                <input type="radio" name="trip" value="Round-trip"><span>Roud-Trip</span>
                                <input type="radio" name="trip" value="onw-way"><span>One-way</span>
                            </div>

                            <div class="location clearfix">
                                <div class="pull-left">
                                    <label>Your Locatıon</label>
									<select name='location'>
									<option value="">Chengdu</option>
                              <option value="">Shanghai</option>
                              <option value="">Beijing</option>
                              <option value="">Guangzhou</option>
									</select>
									
                                    <!--input type="input" name="Location" value="Chengdu"-->
                                </div>
                                <div class="pull-right">
                                    <label class="dst">Destination</label>
                                    <select name='location'>
									<option value="">Chengdu</option>
                              <option value="">Shanghai</option>
                              <option value="">Beijing</option>
                              <option value="">Guangzhou</option>
							  <option value="">Xichang</option>
									</select>
                                </div>
                            </div>

                            <div class="location clearfix">

                                <div class="pull-left">
                                    <div class="date clearfix">
                                        <div class="Depart-Date">
                                            <label>Go-off Date</label>
                                            <input type="text" name="Location" value="05/07/2017" id="datepicker">
                                        </div>
                                        <div>
                                            <label>Reach Date</label>
                                            <input type="text" name="Location" value="05/07/2017" id="clender">
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="persons">
                                        <div class="ad">
                                            <label>Adults</label>
                                            <input type="text" name="Location" value="1" id="spinner">
                                        </div>
                                        <div class="ad">
                                            <label>Seat ID</label>
                                            <input type="text" name="Location" value="1" id="spinner-two">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="search">
                                <input type="submit" name="search" value="Search" >
                            </div>
                        </form>
                      </div>
                      
                      
                   </div>
                    <!-- Reservation box -->
                    
                </div>
                <!-- Slider -->

                <!-- Special Offer -->
               	<div class="specialoffer-wrapper">
                	<div class="container">
                    	
                        <!-- Heading -->
                        <div class="row">
                        	<div class="span12">
                            	<div class="heading">
                                	<h2>China <span>Railway</span></h2>
                                </div>
                            </div>
                        </div>
                        <!-- Heading -->
                         
                       
                <!-- Special Offer -->
                
                <!-- Testimonial -->
               	<div class="testimonial-wrapper">
                	<div class="container">
                    	<div class="row">
                        	<div class="span12 test">
                            
                            	<div class="testimonial">
                                	<div>
                                		<span class="comas"></span>
                                		<p>China's total railway mileage of 121 thousand kilometers, ranking the world's largest in the world, including high-speed railway 19 thousand km, ranking first in the world.</p>
                                		<figure>
                                			<img src="images/biaozhi.jpg" alt="PIc"> 
                                		</figure>
                                		<p class="client">China</p>
                                		<span>railway network</span>
                                	</div>
                                    <div>
                                		<span class="comas"></span>
                                		<p>In China, the railway is an important infrastructure of the country, the most popular transportation.</p>
                                		<figure>
                                			<img src="images/biaozhi.jpg" alt="PIc"> 
                                		</figure>
                                		<p class="client">China</p>
                                		<span>railway network</span>
                                	</div>
                                </div>
                                <div class="test-button">
                                	<a href="#" id="test-prev"></a>
                                	<a href="#" id="test-next"></a>
                                </div>
                            	
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Testimonial -->

                <!-- Footer widget -->
                <div class="footer-widget-wrapper">
                	<div class="container">
                    	<div class="row">
                        	
                            <div id="111" class="span3 f-widget copy-right">
                            	<a href="#" class="f-logo"><img src="images/temp.png" alt="Logo">China-Railway</a>
                            	<p>© 2017 <a href="#">China-Railway</a>. All rights reserved</p>
                            	<p>Designed by Neos</p>
                            </div>
                            <div class="span3 f-widget">
                            	<h4>Designer Infomation</h4>
                                <ul>
                                	<li><a href="#">About US</a></li>
                                	<li><a href="#">Team</a></li>
                                	<li><a href="#">Tips</a></li>
                                	<li><a href="#">Instructions</a></li>                      	
                                </ul>
                            </div>
                            
                            <div class="span3 f-widget">
                            	<div class="cc">
                            		<h4>Contact number</h4>
                            		<h2>028-888-8866-6</h2>
                            		<span class="pull-right">2017/5</span>
                            	</div>
                            	<div class="cc">
                            		<h4>Contact E-mail</h4>
                            		<h2>yishujia1225@163.com</h2>
                            		<span class="pull-right">2017/5</span>
                            	</div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- Footer widget -->
                
                <!-- Footer -->
                <div class="footer-wrapper">
                	<div class="container">
                    	<div class="row">
                        	<div class="span12">
                            
                            	<footer>
                                	<div class="footer-nav">
                                    	<ul>
                                    		<li><a href="#">About US</a></li>
                                    		<li><a href="#">News</a></li>
                                    		<li><a href="#">Serbice</a></li>
                                    		<li><a href="#">Recruiment </a></li>
                                    		<li><a href="#">Media</a></li>
                                    		
                                    	</ul>
                                    </div>
                                    <a href="javascript:void(0)" onClick="goToByScroll('top')" class="gotop"></a>
                                </footer>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer -->
                
                <div class="login-popup-wrapper">
                    <div id="login-popup">
                    	<h2>login Panel</h2>
                        <form method="get" action="#">
                            <input type="text" value="" id="username" placeholder="kavinhieu@gmail.com" />
                            <input type="text" value="" id="password" placeholder="Password" />
                            
                            <input type="submit" value="sıgn ın" id="login-button"/>
                        </form>
                        <a href="#" class="close">Close</a>
                    </div>
                </div>



                <!-- Scripts -->
				<script src="js/jquery-1.7.1.min.js"></script>
				<script src="js/jquery.flexslider.js"></script>
                <script src="js/jquery.flexslider-min.js"></script>
                <script src="js/jquery.elastislide.js"></script>
                <script src="js/jquery.carouFredSel-6.0.4-packed.js"></script>
                <script src="js/jcarousellite_1.0.1.js"></script>
                <script src="js/jquery.zweatherfeed.js"></script>
                <script src="js/jquery.simpleWeather-2.3.min.js"></script>
                <script src="js/jquery.cycle.all.js"></script>
                <script src="js/jquery-ui.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery.isotope.min.js"></script>
                <script src="js/jquery.tinyscrollbar.min.js"></script>
                <script>
                    function goToByScroll(id){
                        $ = jQuery;
                        $('html,body').animate({scrollTop: $("#"+id).offset().top},3000);
                    }
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.scrollbar1').tinyscrollbar();
                    });
                </script>
                <script src="js/custom.js"></script>		
		</body>
</html>