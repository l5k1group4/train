 <?php 
require('db.php');
if (isset($_POST['ticket_id'])  && isset($_POST['ticket_type']) && isset($_POST['ticket_trainid']) && isset($_POST['ticket_seatid']) && isset($_POST['ticket_seattype']) && isset($_POST['ticket_starttime']) && isset($_POST['ticket_reachtime']) && isset($_POST['ticket_startplace']) && isset($_POST['ticket_destination']) && isset($_POST['ticket_price']) && isset($_POST['ticket_status'])) {

	// Save info form $_POST to local variables	
	
	$ticket_id = $_POST['ticket_id'];
	$ticket_type = $_POST['ticket_type'];
	$ticket_trainid = $_POST['ticket_trainid'];
	$ticket_seatid = $_POST['ticket_seatid'];
	$ticket_seattype = $_POST['ticket_seattype'];
	$ticket_starttime = $_POST['ticket_starttime'];
	$ticket_reachtime = $_POST['ticket_reachtime'];
	$ticket_startplace = $_POST['ticket_startplace'];
	$ticket_destination = $_POST['ticket_destination'];
	$ticket_price= $_POST['ticket_price'];
	$ticket_status = $_POST['ticket_status'];
    // Create SQL and execute query
	
$sql = "insert into ticket (id, type, trainid, seatid, seattype, starttime, reachtime, startplace, destination, price, status) values ('$ticket_id', '$ticket_type', '$ticket_trainid', '$ticket_seatid', '$ticket_seattype','$ticket_starttime', '$ticket_reachtime','$ticket_startplace', '$ticket_destination', '$ticket_price','$ticket_status')";
$result = mysql_query($sql) or die(mysql_error());
header('Location: tables.php');
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS - Add ticket</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap1.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Log out</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="CMS.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="charts.php"><i class="fa fa-bar-chart-o"></i> Trainlist</a></li>
            <li class="active"><a href="tables.php"><i class="fa fa-table"></i> Ticketlist</a></li>
            <li><a href="forms.php"><i class="fa fa-edit"></i> Orderlist</a></li>
            <li><a href="customerlist.php"><i class="fa fa-edit"></i> Customerlist</a></li>
			<li  class=""><a href="conductor.php"><i class="fa fa-edit"></i> Conductorlist</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown messages-dropdown">
             
             
            </li>
            <li class="dropdown alerts-dropdown">
              
            </li>
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Neos <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Ticketlist <small>manage Your ticketlist</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="fa fa-dashboard"></i> CMS</a></li>
              <li class="active"><i class="fa fa-table"></i> Ticketlist</li>
            </ol>
            
          </div>
        </div><!-- /.row -->

       
<body>

<form action="addticket.php" method="post">
<div class="form-group" >
                <label>ID</label>
                <input class="form-control" type="text" name="ticket_id" >
              </div>
<div class="form-group" >
                <label>Type</label>
                <input class="form-control" type="text" name="ticket_type" >
              </div>
              <div class="form-group" >
                <label>Trainid</label>
                <input class="form-control" type="text" name="ticket_trainid" >
              </div>
             <div class="form-group" >
                <label>Seatid</label>
                <input class="form-control" type="text" name="ticket_seatid" >
              </div>
			  <div class="form-group" >
                <label>Seattype</label>
                <input class="form-control" type="text" name="ticket_seattype" >
              </div>
             <div class="form-group" >
                <label>Starttime</label>
                <input class="form-control" type="text" name="ticket_starttime" >
              </div>
              <div class="form-group" >
                <label>Reachtime</label>
                <input class="form-control" type="text" name="ticket_reachtime" >
              </div>
        <div class="form-group" >
                <label>Startplace</label>
                <input class="form-control" type="text" name="ticket_startplace" >
              </div>
			  <div class="form-group" >
                <label>Destination</label>
                <input class="form-control" type="text" name="ticket_destination" >
              </div>
			  <div class="form-group" >
                <label>Price</label>
                <input class="form-control" type="text" name="ticket_price" >
              </div>
			  <div class="form-group" >
                <label>Status</label>
                <input class="form-control" type="text" name="ticket_status" >

              </div>
<input type="submit" value="Add" class="btn btn-danger" a href="tables.php">
</form>

        </div><!-- /.row -->


      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

    <!-- Page Specific Plugins -->
    <script src="js/tablesorter/jquery.tablesorter.js"></script>
    <script src="js/tablesorter/tables.js"></script>

  </body>
</html>