<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS - Orderlist</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap1.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Log out</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="CMS.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="charts.php"><i class="fa fa-bar-chart-o"></i> Trainlist</a></li>
            <li><a href="tables.php"><i class="fa fa-table"></i> Ticketlist</a></li>
            <li class="active"><a href="forms.php"><i class="fa fa-edit"></i> Orderlist</a></li>
            <li><a href="customerlist.php"><i class="fa fa-edit"></i> Customerlist</a></li>
			<li  class=""><a href="conductor.php"><i class="fa fa-edit"></i> Conductorlist</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown messages-dropdown">
              
              <ul class="dropdown-menu">
               
                
              </ul>
            </li>
            <li class="dropdown alerts-dropdown">
              <
              <ul class="dropdown-menu">
                <li><a href="#">Default <span class="label label-default">Default</span></a></li>
                <li><a href="#">Primary <span class="label label-primary">Primary</span></a></li>
                <li><a href="#">Success <span class="label label-success">Success</span></a></li>
                <li><a href="#">Info <span class="label label-info">Info</span></a></li>
                <li><a href="#">Warning <span class="label label-warning">Warning</span></a></li>
                <li><a href="#">Danger <span class="label label-danger">Danger</span></a></li>
                <li class="divider"></li>
                <li><a href="#">View All</a></li>
              </ul>
            </li>
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Neos <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Forms <small>Enter Your Data</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
              <li class="active"><i class="fa fa-edit"></i> Forms</li>
            </ol>
            
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-6">
            <h2>Order List Table</h2>
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th>ID <i class="fa fa-sort"></i></th>
                    <th>Ticketid <i class="fa fa-sort"></i></th>
                    <th>Customerid <i class="fa fa-sort"></i></th>
                    <th>Conductorid <i class="fa fa-sort"></i></th>				
					<th>Trainid <i class="fa fa-sort"></i></th>
					<th>Seatid <i class="fa fa-sort"></i></th>
					<th>Update <i class="fa fa-sort"></i></th>
					<th>Delete <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
				<?php
require("db.php");


$result=mysql_query("select * from orderlist ");

 while ($row = mysql_fetch_array($result)){
?>
                <tbody>
                  <tr>
                    <td ><?php echo $row{'id'} ?></td>
		<td ><?php echo $row{'ticketid'} ?></td>
		<td ><?php echo $row{'customerid'} ?></td>
		<td ><?php echo $row{'conductorid'} ?></td>
		<td ><?php echo $row{'trainid'} ?></td>
		<td ><?php echo $row{'seatid'} ?></td>
		
		<td > <a href="updateorderlist.php?id=<?php echo $row{'id'}?>">Update</a></td>
		<td > <a href="deleteorder.php?id=<?php echo $row{'id'}?>">Delete</a></td>
                  </tr>
                  
                </tbody>

		  
          <?php
}

mysql_close($dbhandle);
?>

              </table>
            </div>
          </div>

        </div><!-- /.row -->
        <a href="addorderlist.php" class="btn btn-danger">Add order</a>

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

  </body>
</html>